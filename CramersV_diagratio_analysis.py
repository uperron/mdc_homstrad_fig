# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import numpy as np
import pandas as pd
from collections import defaultdict
import itertools
from scipy.stats import chi2_contingency
import matplotlib
from matplotlib import pyplot as plt
import matplotlib.patches as mpatches
import seaborn as sb

matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
plt.style.use(['seaborn-white', 'seaborn-paper'])
matplotlib.rc("font", serif='Helvetica Neue')

# from:/nfs/research1/goldman/umberto/bioinfotree/prj/MDC_homstrad/dataset/1
# https://bitbucket.org/uperron/mdc_homstrad
assoc_f = "data/merged.ALLFAMS_accessibility_T.rota.counts_Dnorm.block_association_tab"
assoc_df = pd.read_csv(assoc_f, sep="\t", header=0)
# -

assoc_df.head()

# +

fig, ax = plt.subplots(figsize=(10,10))
# filter on Bonferroni corrected P_val of the chi_squared test
# and Cramer's V
nozero_data = assoc_df[(assoc_df.Bonferroni_pval < .05) & (assoc_df.CramersV > .3)]

hydrophobic_aa = ["CYS", "MET", "ILE", "LEU", "VAL", "PHE"]

arr = []
for a1, a2 in nozero_data[["a1","a2"]].values:
    if a1 in hydrophobic_aa and a2 in hydrophobic_aa:
        v = "Both hydrophobic"
    elif a1 not in hydrophobic_aa and a2 not in hydrophobic_aa:
        v = "Both polar"
    else:
        v = "Mixed"
    arr.append(v)
nozero_data["hydro"] = arr

# convert AA names to one letter codes
three_one_dict = dict(zip("ALA,ARG,ASN,ASP,CYS,GLN,GLU,GLY,HIS,ILE,LEU,LYS,MET,PHE,PRO,SER,THR,TRP,TYR,VAL".split(","),
                          "A,R,N,D,C,Q,E,G,H,I,L,K,M,F,P,S,T,W,Y,V".split(",")))
nozero_data.loc[:, "sl1"] =  nozero_data['a1'].map(three_one_dict)
nozero_data.loc[:, "sl2"] =  nozero_data['a2'].map(three_one_dict)

cmap = sb.cubehelix_palette(dark=.3, light=.8, as_cmap=True)
sb.scatterplot(x='CramersV', y='diagonal_ratio', hue="type", style="hydro",
               size="Bonferroni_pval", size_norm=(50,100), data=nozero_data, ax=ax)

for label, x_val, y_val in zip(nozero_data["sl1"] + "," + nozero_data["sl2"], 
                               nozero_data['CramersV'].tolist(), nozero_data['diagonal_ratio'].tolist()):
            plt.annotate(label, xy=(x_val, y_val), xytext=(15, -15), textcoords='offset points', 
                         ha='right', va='bottom', fontsize='medium', arrowprops=dict(arrowstyle = '-', 
                                                                  connectionstyle='arc3,rad=0'))  

ax.set_xlabel('Cramer\'s V', fontsize='large')
ax.set_ylabel('Diagonal ratio', fontsize='large')
#plt.legend(patches, patch_labels, ncol=2, loc = 'lower right',fontsize='medium' )

plt.show()
fig.savefig('figures/CramersV_diagratio_.scatter.pdf', format='pdf', bbox_inches='tight', dpi=fig.dpi)


# +
# filter on Bonferroni corrected P_val of the chi_squared test
# and Cramer's V
nozero_data = assoc_df[(assoc_df.Bonferroni_pval < .05) & (assoc_df.CramersV > .3)]

# remove CYS, MET as in RAM55
to_drop = ["ALA", "GLY", "PRO", "CYS", "MET"]
nozero_data = nozero_data.query('a1 not in @to_drop & a2 not in @to_drop')

AAdict = {}
hydrophobic_aa = ["CYS", "MET", "ILE", "LEU", "VAL", "PHE"]
for aa in AA_count_df.columns:
    if aa in hydrophobic_aa:
        AAdict[aa] = "hydrophobic"
    else:
        AAdict[aa] = "polar"

# one color per AA1 - AA2 combination
unique_groups =  list(itertools.combinations_with_replacement(sorted(set(AAdict.values())), 2))


def map_or_reversemap(g_tup):
    if g_tup in unique_groups:
        return "-".join(g_tup)
    else:
        inv_tup = (g_tup[1], g_tup[0])
        return "-".join(inv_tup)

L = list(zip(nozero_data['a1'].map(AAdict).astype(str), nozero_data['a2'].map(AAdict).astype(str)))
           
nozero_data["group_name"] = [map_or_reversemap(l) for l in L]

to_keep = ['aromatic-aromatic', 'negative-negative', 'positive-positive', 
           'aromatic-positive','aliphatic-aromatic', 
           'negative-positive', 'aromatic-carboxyamine', 'carboxyamine-positive', 
           'aliphatic-positive', 'aromatic-negative', 
           'hydroxyl-positive', 'aromatic-hydroxyl']

# convert AA names to one letter codes
three_one_dict = dict(zip("ALA,ARG,ASN,ASP,CYS,GLN,GLU,GLY,HIS,ILE,LEU,LYS,MET,PHE,PRO,SER,THR,TRP,TYR,VAL".split(","),
                          "A,R,N,D,C,Q,E,G,H,I,L,K,M,F,P,S,T,W,Y,V".split(",")))
nozero_data.loc[:, "sl1"] =  nozero_data['a1'].map(three_one_dict)
nozero_data.loc[:, "sl2"] =  nozero_data['a2'].map(three_one_dict)

plt.clf()
fig, ax = plt.subplots(figsize=(12,12))

nozero_data.loc[:,'group_median'] = nozero_data.groupby('group_name')['diagonal_ratio'].transform(np.median)                                                                                              
nozero_data.sort_values(['group_median'], ascending=[True], inplace=True)
# remove small groups
groups = nozero_data.groupby('group_name', sort=False)
groups = groups.filter(lambda x: len(x) > 0).groupby('group_name', sort=False)

y=[]
l = []
for name, group in groups:
    y.append(group.loc[:, 'diagonal_ratio'].values)
    l.append(name)
np.array(y)

colors = sb.color_palette("colorblind", len(l), desat=.9)

bp = ax.boxplot(y, patch_artist=True, widths=.5, positions=range(len(l)))
for box, c in zip(bp['boxes'], colors):
    # change outline color
    box.set( color='#000000', linewidth=0.5)
    # change fill color
    box.set(facecolor = c)
for median in bp['medians']:
    median.set(color='k', linewidth=0.9)
## change the style of fliers and their fill
for flier in bp['fliers']:
    flier.set(marker='', color=c)

plt.style.use(['seaborn-white', 'seaborn-paper'])
matplotlib.rc("font", serif="Helvetica Neue")
ax.set_ylabel('Diagonal ratio', fontsize='large')
ax.tick_params(axis='both', length=3)
ax.set_xticks(range(len(l)))
ax.set_xticklabels(l, rotation=30, fontsize='medium', ha='right')
plt.show()
fig.savefig('figures/CramersV_diagratio.boxplots.svg', format='svg', bbox_inches='tight', dpi=fig.dpi);
