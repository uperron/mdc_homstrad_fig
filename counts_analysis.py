# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sb

matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
plt.style.use(['seaborn-white', 'seaborn-paper'])
matplotlib.rc("font", serif='Helvetica Neue')

colors = sb.cubehelix_palette(5, start=.5, rot=-.7, light=.97)

accF_AA_count_f = "data/merged.ALLFAMS_accessibility_F.AA.counts"
accF_AA_count_df = pd.read_csv(accF_AA_count_f, sep="\t", header=0, index_col=0)
accF_rota_count_f = "data/merged.ALLFAMS_accessibility_F.rota.counts"
accF_rota_count_df = pd.read_csv(accF_rota_count_f, sep="\t", header=0, index_col=0)
accT_AA_count_f = "data/merged.ALLFAMS_accessibility_T.AA.counts"
accT_AA_count_df = pd.read_csv(accT_AA_count_f, sep="\t", header=0, index_col=0)
accT_rota_count_f = "data/merged.ALLFAMS_accessibility_T.rota.counts"
accT_rota_count_df = pd.read_csv(accT_rota_count_f, sep="\t", header=0, index_col=0)

# sum across each count matrix column
arr = []
for S in accT_rota_count_df.columns:
    F_counts = accF_rota_count_df[S].sum()
    T_counts = accT_rota_count_df[S].sum()
    arr.append([S, F_counts, T_counts, F_counts + T_counts])

rota_sumcounts_df = pd.DataFrame(arr, columns=["state", "acc_F", "acc_T", "tot"])

rota_sumcounts_df.head()

# +

fig, ax = plt.subplots(1, figsize=(16,6))

# plot buried vs exposed counts for each rotastate
sb.barplot(x = rota_sumcounts_df.state, y = rota_sumcounts_df.tot, 
           color = colors[1], ax=ax)
sb.barplot(x = rota_sumcounts_df.state, y = rota_sumcounts_df.acc_F, 
           color = colors[3], ax=ax)

topbar = plt.Rectangle((0,0),1,1,fc=colors[1], edgecolor = 'none')
bottombar = plt.Rectangle((0,0),1,1,fc=colors[3],  edgecolor = 'none')
l = plt.legend([bottombar, topbar], ['Buried', 'Exposed'], loc=1,
               ncol = 2, prop={'size':12})
l.draw_frame(False)

ax.set_ylabel("Counts", fontsize="large")
ax.set_xlabel("State", fontsize="large")
ax.set_xticklabels(accT_rota_count_df.columns, fontsize="xx-small", rotation=30)
fig.savefig("figures/Buried_v_exposed_counts.stackplot.pdf", format='pdf', 
            bbox_inches='tight', dpi=fig.dpi)
plt.show()

# +
from collections import defaultdict


to_drop = ["ALA", "GLY"]

AAdict = {'GLN': 'carboxyamine', 'HIS': 'positive', 'SER': 'hydroxyl',
'VAL': 'aliphatic', 'LYS': 'positive', 'ILE': 'aliphatic', 'ASN': 'carboxyamine',
'THR': 'hydroxyl', 'PHE': 'aromatic', 'ASP': 'negative', 'LEU': 'aliphatic', 'ARG': 'positive',
          'TRP': 'aromatic', 'GLU': 'negative', 'TYR': 'aromatic'
}

orig_df = rota_sumcounts_df
orig_df["AA"] = [r[:3] if r not in to_drop else r 
                           for r in orig_df["state"] ]
orig_df = orig_df.query('AA not in @to_drop')

reshaped_dict = defaultdict(list)
for k, v in zip(AAdict.values(), AAdict.keys()):
    reshaped_dict[k].append(v)
reshaped_dict["hydrophobic"] = ["PRO", "CYS", "MET", "ILE", "LEU", "VAL", "PHE"]
reshaped_dict["polar"] = [a for a in orig_df["AA"].unique() 
                          if a not in reshaped_dict["hydrophobic"]]
arr = []
for k in reshaped_dict.keys():
    AA_list = reshaped_dict[k]
    df = orig_df.query('AA in @AA_list')
    arr.append([k, df['acc_F'].sum(), df['tot'].sum()])
group_orig_df = pd.DataFrame(arr, columns=["group", "acc_F", "tot"])


fig, ax = plt.subplots(1, figsize=(12,6))

# plot buried vs exposed counts for each biochemical grouping of residues
sb.barplot(x = group_orig_df.group, y = group_orig_df.tot, 
           color = colors[1], ax=ax)
sb.barplot(x = group_orig_df.group, y = group_orig_df.acc_F, 
           color = colors[3], ax=ax)

topbar = plt.Rectangle((0,0),1,1,fc=colors[1], edgecolor = 'none')
bottombar = plt.Rectangle((0,0),1,1,fc=colors[3],  edgecolor = 'none')
l = plt.legend([bottombar, topbar], ['Buried', 'Exposed'], loc=2,
               ncol = 2, prop={'size':12})
l.draw_frame(False)

ax.set_ylabel("Counts", fontsize="large")
ax.set_xlabel("Group", fontsize="large")
ax.set_xticklabels(group_orig_df.group, fontsize="small", rotation=30)
fig.savefig("figures/Buried_v_exposed_counts.AA_grouped.stackplot.pdf", format='pdf', 
            bbox_inches='tight', dpi=fig.dpi)
plt.show()

# +
orig_df = rota_sumcounts_df
orig_df = orig_df.query('AA not in @to_drop')
orig_df["chi_1"] = [r[3:] for r in orig_df["state"] ]


arr = []
for k in orig_df["chi_1"].unique():
    df = orig_df[orig_df["chi_1"] == k]
    arr.append([k, df['acc_F'].sum(), df['tot'].sum()])
group_orig_df = pd.DataFrame(arr, columns=["group", "acc_F", "tot"])


fig, ax = plt.subplots(1, figsize=(12,6))

# plot buried vs exposed counts for each chi_1 configuration
sb.barplot(x = group_orig_df.group, y = group_orig_df.tot, 
           color = colors[1], ax=ax)
sb.barplot(x = group_orig_df.group, y = group_orig_df.acc_F, 
           color = colors[3], ax=ax)

topbar = plt.Rectangle((0,0),1,1,fc=colors[1], edgecolor = 'none')
bottombar = plt.Rectangle((0,0),1,1,fc=colors[3],  edgecolor = 'none')
l = plt.legend([bottombar, topbar], ['Buried', 'Exposed'], loc=2,
               ncol = 2, prop={'size':12})
l.draw_frame(False)

ax.set_ylabel("Counts", fontsize="large")
ax.set_xlabel("Group", fontsize="large")
ax.set_xticklabels(orig_df["chi_1"].unique(), fontsize="small", rotation=30)
fig.savefig("figures/Buried_v_exposed_counts.AA_config.stackplot.pdf", format='pdf', 
            bbox_inches='tight', dpi=fig.dpi)
plt.show()
