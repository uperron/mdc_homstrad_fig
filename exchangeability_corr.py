# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import pandas as pd
import numpy as np
from scipy import stats
import itertools
from collections import defaultdict
import matplotlib
from matplotlib import pyplot as plt
import matplotlib.patches as mpatches
import seaborn as sb

matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
plt.style.use(['seaborn-white', 'seaborn-paper'])
matplotlib.rc("font", serif='Helvetica Neue')

# correlation b/w columns in the exchangeability matrix in different JOY classes
df1 = pd.read_csv("data/merged.ALLFAMS_solvent_accessibility_T.rota.exchangeabilities_superQ",
                 sep="\t", index_col=0, header=0)
df2 = pd.read_csv("data/merged.ALLFAMS_solvent_accessibility_F.rota.exchangeabilities_superQ",
                 sep="\t", index_col=0, header=0)

ROTA_corr_S = df1.corrwith(df2)
sb.distplot(ROTA_corr_S, label="solvent_accessibility");
print(ROTA_corr_S.sort_values().head())

df1 = pd.read_csv("data/merged.ALLFAMS_secondary_structure_and_phi_angle_E.rota.exchangeabilities_superQ",
                 sep="\t", index_col=0, header=0)
df2 = pd.read_csv("data/merged.ALLFAMS_secondary_structure_and_phi_angle_H.rota.exchangeabilities_superQ",
                 sep="\t", index_col=0, header=0)    
ROTA_corr_S = df1.corrwith(df2)
sb.distplot(ROTA_corr_S, label="secondary_structure");
plt.legend();
plt.show();
print(ROTA_corr_S.sort_values().head())

# +
# correlation b/w 3x3 blocks in different JOY classes
AAs = 'GLY,ALA,PRO,CYS,MET,ILE,LEU,VAL,PHE,TRP,\
TYR,HIS,ARG,LYS,GLN,ASN,ASP,GLU,SER,THR'.split(",")
ROTAs = 'GLY,ALA,PRO1,PRO2,CYS1,CYS2,CYS3,MET1,MET2,\
MET3,ILE1,ILE2,ILE3,LEU1,LEU2,LEU3,VAL1,VAL2,VAL3,PHE1,\
PHE2,PHE3,TRP1,TRP2,TRP3,TYR1,TYR2,TYR3,HIS1,HIS2,HIS3,\
ARG1,ARG2,ARG3,LYS1,LYS2,LYS3,GLN1,GLN2,GLN3,ASN1,ASN2,\
ASN3,ASP1,ASP2,ASP3,GLU1,GLU2,GLU3,SER1,SER2,SER3,THR1,\
THR2,THR3'.split(",")


state_dict = defaultdict(list)
for aa in AAs:
    state_dict[aa].extend([r for r in ROTAs if aa in r])

df1 = pd.read_csv("data/merged.ALLFAMS_solvent_accessibility_T.rota.exchangeabilities_superQ",
                 sep="\t", index_col=0, header=0)
df2 = pd.read_csv("data/merged.ALLFAMS_solvent_accessibility_F.rota.exchangeabilities_superQ",
                 sep="\t", index_col=0, header=0)    
    
square_labels = [a for a in AAs if a not in ["ALA", "GLY", "PRO"]]
square_pairs = list(itertools.combinations(square_labels, 2))
numtest = len(square_pairs)

arr = []
for a1,a2 in square_pairs:
        # slice the corresponding rotamer submatrix
        X = df1.loc[state_dict[a1],state_dict[a2]].values.flatten()
        Y = df2.loc[state_dict[a1],state_dict[a2]].values.flatten()
        r, pval = stats.pearsonr(X, Y)
        arr.append([a1, a2, r, pval])
AA_corr_df = pd.DataFrame(arr, columns=["AA1", "AA2", "Pearson_r", "p-value"])
# filter on r p-val
AA_corr_df = AA_corr_df[AA_corr_df["p-value"] < .05]

mx = AA_corr_df.Pearson_r.max()
mn = AA_corr_df.Pearson_r.min()
bins=np.linspace(mn, mx, 10, endpoint=False)
sb.distplot(AA_corr_df.Pearson_r, bins=bins, kde=False, label="solvent_accessibility");

print(AA_corr_df.sort_values(by=["Pearson_r"]).head(10));

###################

state_dict = defaultdict(list)
for aa in AAs:
    state_dict[aa].extend([r for r in ROTAs if aa in r])

df1 = pd.read_csv("data/merged.ALLFAMS_secondary_structure_and_phi_angle_E.rota.exchangeabilities_superQ",
                 sep="\t", index_col=0, header=0)
df2 = pd.read_csv("data/merged.ALLFAMS_secondary_structure_and_phi_angle_H.rota.exchangeabilities_superQ",
                 sep="\t", index_col=0, header=0)    
    
square_labels = [a for a in AAs if a not in ["ALA", "GLY", "PRO"]]
square_pairs = list(itertools.combinations(square_labels, 2))
numtest = len(square_pairs)

arr = []
for a1,a2 in square_pairs:
        # slice the corresponding rotamer submatrix
        X = df1.loc[state_dict[a1],state_dict[a2]].values.flatten()
        Y = df2.loc[state_dict[a1],state_dict[a2]].values.flatten()
        r, pval = stats.pearsonr(X, Y)
        arr.append([a1, a2, r, pval])
AA_corr_df = pd.DataFrame(arr, columns=["AA1", "AA2", "Pearson_r", "p-value"])
# filter on r p-val
AA_corr_df = AA_corr_df[AA_corr_df["p-value"] < .05]

sb.distplot(AA_corr_df.Pearson_r, bins=bins, kde=False, label="secondary_structure");
plt.legend();
plt.show();
print(AA_corr_df.sort_values(by=["Pearson_r"]).head(10));

# +
AAs = 'GLY,ALA,PRO,CYS,MET,ILE,LEU,VAL,PHE,TRP,\
TYR,HIS,ARG,LYS,GLN,ASN,ASP,GLU,SER,THR'.split(",")
ROTAs = 'GLY,ALA,PRO1,PRO2,CYS1,CYS2,CYS3,MET1,MET2,\
MET3,ILE1,ILE2,ILE3,LEU1,LEU2,LEU3,VAL1,VAL2,VAL3,PHE1,\
PHE2,PHE3,TRP1,TRP2,TRP3,TYR1,TYR2,TYR3,HIS1,HIS2,HIS3,\
ARG1,ARG2,ARG3,LYS1,LYS2,LYS3,GLN1,GLN2,GLN3,ASN1,ASN2,\
ASN3,ASP1,ASP2,ASP3,GLU1,GLU2,GLU3,SER1,SER2,SER3,THR1,\
THR2,THR3'.split(",")


state_dict = defaultdict(list)
for aa in AAs:
    state_dict[aa].extend([r for r in ROTAs if aa in r])

df1 = pd.read_csv("data/merged.ALLFAMS_solvent_accessibility_T.rota.exchangeabilities_superQ",
                 sep="\t", index_col=0, header=0)
df2 = pd.read_csv("data/merged.ALLFAMS_solvent_accessibility_F.rota.exchangeabilities_superQ",
                 sep="\t", index_col=0, header=0)    
    
square_labels = [a for a in AAs if a not in ["ALA", "GLY", "PRO"]]
square_pairs = list(itertools.combinations(square_labels, 2))
numtest = len(square_pairs)

arr = []
for a1,a2 in square_pairs:
        # slice the corresponding rotamer submatrix
        X = df1.loc[state_dict[a1],state_dict[a2]].values.flatten()
        Y = df2.loc[state_dict[a1],state_dict[a2]].values.flatten()
        r, pval = stats.pearsonr(X, Y)
        arr.append([a1, a2, r, pval])
AA_corr_df = pd.DataFrame(arr, columns=["a1", "a2", "Pearson_r", "p-value"])
# filter on r p-val
AA_corr_df = AA_corr_df[AA_corr_df["p-value"] < .05]

assoc_f1 = "data/merged.ALLFAMS_solvent_accessibility_T.rota.counts_Dnorm.block_association_tab"
assoc_f2 = "data/merged.ALLFAMS_solvent_accessibility_F.rota.counts_Dnorm.block_association_tab"
assoc_df1 = pd.read_csv(assoc_f1, sep="\t", header=0)
assoc_df1 =  assoc_df1[assoc_df1.CramersV > .3]
assoc_df2 = pd.read_csv(assoc_f2, sep="\t", header=0)
assoc_df2 =  assoc_df2[assoc_df2.CramersV > .3]
assoc_df = pd.merge(assoc_df1, assoc_df2, on=["a1", "a2"], suffixes=["_T", "_F"])
assoc_df =  pd.merge(assoc_df, AA_corr_df, on=["a1", "a2"])
assoc_df = assoc_df.sort_values(by="CramersV_F")

hydrophobic_aa = ["CYS", "MET", "ILE", "LEU", "VAL", "PHE"]
arr = []
for a1, a2 in assoc_df[["a1","a2"]].values:
    if a1 in hydrophobic_aa and a2 in hydrophobic_aa:
        v = "Both hydrophobic"
    elif a1 not in hydrophobic_aa and a2 not in hydrophobic_aa:
        v = "Both polar"
    else:
        v = "Mixed"
    arr.append(v)
assoc_df["hydro"] = arr

# convert AA names to one letter codes
three_one_dict = dict(zip("ALA,ARG,ASN,ASP,CYS,GLN,GLU,GLY,HIS,ILE,LEU,LYS,MET,PHE,PRO,SER,THR,TRP,TYR,VAL".split(","),
                          "A,R,N,D,C,Q,E,G,H,I,L,K,M,F,P,S,T,W,Y,V".split(",")))
assoc_df.loc[:, "sl1"] =  assoc_df['a1'].map(three_one_dict)
assoc_df.loc[:, "sl2"] =  assoc_df['a2'].map(three_one_dict)

fig, ax = plt.subplots(figsize=(8,8))
sb.scatterplot(x='CramersV_F', y='CramersV_T', hue="Pearson_r", style="hydro",
               data=assoc_df, ax=ax)
i = 1
j = -1
for label, x_val, y_val in zip(assoc_df["sl1"] + "," + assoc_df["sl2"], 
                               assoc_df['CramersV_F'].tolist(), 
                               assoc_df['CramersV_T'].tolist()):
            plt.annotate(label, xy=(x_val, y_val), xytext=(j * 15, i * 15), textcoords='offset points', 
                         ha='right', va='bottom', fontsize='medium', arrowprops=dict(arrowstyle = '-', 
                                                                  connectionstyle='arc3,rad=0'))
            i = i * -1
            j = j * -1

ax.set_xlabel('Cramer\'s V exposed', fontsize='large')
ax.set_ylabel('Cramer\'s V buried', fontsize='large')
fig.savefig('figures/solvent_accessibility_CramersVs_corr.scatter.pdf', format='pdf', bbox_inches='tight', dpi=fig.dpi);
plt.show();

# +
AAs = 'GLY,ALA,PRO,CYS,MET,ILE,LEU,VAL,PHE,TRP,\
TYR,HIS,ARG,LYS,GLN,ASN,ASP,GLU,SER,THR'.split(",")
ROTAs = 'GLY,ALA,PRO1,PRO2,CYS1,CYS2,CYS3,MET1,MET2,\
MET3,ILE1,ILE2,ILE3,LEU1,LEU2,LEU3,VAL1,VAL2,VAL3,PHE1,\
PHE2,PHE3,TRP1,TRP2,TRP3,TYR1,TYR2,TYR3,HIS1,HIS2,HIS3,\
ARG1,ARG2,ARG3,LYS1,LYS2,LYS3,GLN1,GLN2,GLN3,ASN1,ASN2,\
ASN3,ASP1,ASP2,ASP3,GLU1,GLU2,GLU3,SER1,SER2,SER3,THR1,\
THR2,THR3'.split(",")


state_dict = defaultdict(list)
for aa in AAs:
    state_dict[aa].extend([r for r in ROTAs if aa in r])

df1 = pd.read_csv("data/merged.ALLFAMS_secondary_structure_and_phi_angle_H.rota.exchangeabilities_superQ",
                 sep="\t", index_col=0, header=0)
df2 = pd.read_csv("data/merged.ALLFAMS_secondary_structure_and_phi_angle_E.rota.exchangeabilities_superQ",
                 sep="\t", index_col=0, header=0)    
    
square_labels = [a for a in AAs if a not in ["ALA", "GLY", "PRO"]]
square_pairs = list(itertools.combinations(square_labels, 2))
numtest = len(square_pairs)

arr = []
for a1,a2 in square_pairs:
        # slice the corresponding rotamer submatrix
        X = df1.loc[state_dict[a1],state_dict[a2]].values.flatten()
        Y = df2.loc[state_dict[a1],state_dict[a2]].values.flatten()
        r, pval = stats.pearsonr(X, Y)
        arr.append([a1, a2, r, pval])
AA_corr_df = pd.DataFrame(arr, columns=["a1", "a2", "Pearson_r", "p-value"])
# filter on r p-val
AA_corr_df = AA_corr_df[AA_corr_df["p-value"] < .05]

assoc_f1 = "data/merged.ALLFAMS_secondary_structure_and_phi_angle_H.rota.counts_Dnorm.block_association_tab"
assoc_f2 = "data/merged.ALLFAMS_secondary_structure_and_phi_angle_E.rota.counts_Dnorm.block_association_tab"
assoc_df1 = pd.read_csv(assoc_f1, sep="\t", header=0)
assoc_df1 =  assoc_df1[assoc_df1.CramersV > .3]
assoc_df2 = pd.read_csv(assoc_f2, sep="\t", header=0)
assoc_df2 =  assoc_df2[assoc_df2.CramersV > .3]

assoc_df = pd.merge(assoc_df1, assoc_df2, on=["a1", "a2"], suffixes=["_H", "_E"])
assoc_df = pd.merge(assoc_df, AA_corr_df, on=["a1", "a2"])
assoc_df = assoc_df.sort_values(by='CramersV_E')

hydrophobic_aa = ["CYS", "MET", "ILE", "LEU", "VAL", "PHE"]
arr = []
for a1, a2 in assoc_df[["a1","a2"]].values:
    if a1 in hydrophobic_aa and a2 in hydrophobic_aa:
        v = "Both hydrophobic"
    elif a1 not in hydrophobic_aa and a2 not in hydrophobic_aa:
        v = "Both polar"
    else:
        v = "Mixed"
    arr.append(v)
assoc_df["hydro"] = arr

# convert AA names to one letter codes
three_one_dict = dict(zip("ALA,ARG,ASN,ASP,CYS,GLN,GLU,GLY,HIS,ILE,LEU,LYS,\
MET,PHE,PRO,SER,THR,TRP,TYR,VAL".split(","),\
"A,R,N,D,C,Q,E,G,H,I,L,K,M,F,P,S,T,W,Y,V".split(",")))

assoc_df.loc[:, "sl1"] =  assoc_df['a1'].map(three_one_dict)
assoc_df.loc[:, "sl2"] =  assoc_df['a2'].map(three_one_dict)

fig, ax = plt.subplots(figsize=(8,8))
cmap = sb.cubehelix_palette(dark=.3, light=.8, as_cmap=True)
sb.scatterplot(x='CramersV_E', y='CramersV_H', hue="Pearson_r", style="hydro",
               data=assoc_df, ax=ax)
i = 1
j = -1
for label, x_val, y_val in zip(assoc_df["sl1"] + "," + assoc_df["sl2"], 
                               assoc_df['CramersV_E'].tolist(), 
                               assoc_df['CramersV_H'].tolist()):
            plt.annotate(label, xy=(x_val, y_val), xytext=(j * 15, i * 15), textcoords='offset points', 
                         ha='right', va='bottom', fontsize='medium', arrowprops=dict(arrowstyle = '-', 
                                                                  connectionstyle='arc3,rad=0'))
            i = i * -1
            j = j * -1

ax.set_xlabel('Cramer\'s V Helix', fontsize='large')
ax.set_ylabel('Cramer\'s V Beta-sheet', fontsize='large')
fig.savefig('figures/secondary_structure_CramersVs_corr.scatter.pdf', format='pdf', bbox_inches='tight', dpi=fig.dpi)
plt.show()
