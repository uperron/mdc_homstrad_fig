# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sb
from ruamel.yaml import YAML
yaml = YAML()

with open("data/state_dicts.yaml", "r") as f:
        data = yaml.load(f)
one_to_three_dict = data["AA_ONE_TO_THREE"]

matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
plt.style.use(['seaborn-white', 'seaborn-paper'])
matplotlib.rc("font", serif='Helvetica Neue')
colors = sb.cubehelix_palette(5, start=.5, rot=-.7, light=.97)

sites_df = pd.read_csv("data/ALLFAMS_rota.JOY_annot.tab", sep ="\t", index_col=None)

sites_df = sites_df[(sites_df["PDB_AA"] != "-") & (sites_df["homstrad_AA"] != "-")]

# overall number of sites with 
# unresolved rotamer configurations 
# in exposed vs buried sites
arr = []
for k in ["F", "T"]:
    df = sites_df[sites_df["solvent accessibility"] == k]
    u = len(df[sites_df["rota_4"] == "-"] )
    arr.append([k, len(df), u])
merged_unresolved_df = pd.DataFrame(arr, columns=["acc", "tot", "unresolved"])


# residue-specific number of unresolved rotamer configurations 
# in exposed vs buried sites
arr = []
for k in sorted(sites_df["PDB_AA"].unique()):
    df = sites_df[sites_df["PDB_AA"] == k]
    u = len(df[df["rota_4"] == "-"])
    F_u = len(df[(df["solvent accessibility"] == "F") & (df["rota_4"] == "-")])
    arr.append([k, len(df), u, F_u])
AA_unresolved_df = pd.DataFrame(arr, columns=["AA", "tot_sites", 
                                              "tot_unresolved", "unresolved_buried"])

# +
fig, ax = plt.subplots(1, figsize=(8,6))


sb.barplot(x = merged_unresolved_df.acc, y = merged_unresolved_df.tot, 
           color = colors[1], ax=ax)
sb.barplot(x = merged_unresolved_df.acc, y = merged_unresolved_df.unresolved, 
           color = colors[2], ax=ax)

topbar = plt.Rectangle((0,0),1,1,fc=colors[1], edgecolor = 'none')
midbar = plt.Rectangle((0,0),1,1,fc=colors[2], edgecolor = 'none')
l = plt.legend([topbar, midbar], ['Total', 'Unresolved'], loc=2,
               ncol = 2, prop={'size':12})
l.draw_frame(False)

ax.set_ylabel("Sites", fontsize="large")
ax.set_xlabel("Solvent accessibility", fontsize="large")
ax.set_xticklabels(merged_unresolved_df.acc, fontsize="small", rotation=30)
fig.savefig("Buried_v_exposed_unresolved_rota.stackplot.pdf", format='pdf', 
            bbox_inches='tight', dpi=fig.dpi)
plt.show()

# +
fig, ax = plt.subplots(1, figsize=(16,6))


sb.barplot(x = AA_unresolved_df.AA, y = AA_unresolved_df.tot_sites, 
           color = colors[1], ax=ax)
sb.barplot(x = AA_unresolved_df.AA, y = AA_unresolved_df.tot_unresolved, 
           color = colors[2], ax=ax)
sb.barplot(x = AA_unresolved_df.AA, y = AA_unresolved_df.unresolved_buried, 
           color = colors[3], ax=ax)

topbar = plt.Rectangle((0,0),1,1,fc=colors[1], edgecolor = 'none')
midbar = plt.Rectangle((0,0),1,1,fc=colors[2], edgecolor = 'none')
bottombar = plt.Rectangle((0,0),1,1,fc=colors[3],  edgecolor = 'none')
l = plt.legend([topbar, midbar, bottombar], ['Total', 'Unresolved', 'Unresolved buried'], loc=1,
               ncol = 2, prop={'size':12})
l.draw_frame(False)

ax.set_ylabel("Sites", fontsize="large")
ax.set_xlabel("amino acid", fontsize="large")
labels = [one_to_three_dict[a] for a in AA_unresolved_df.AA.values]
ax.set_xticklabels(labels, fontsize="xx-small", rotation=30)
fig.savefig("figures/AA_Buried_v_exposed_unresolved_rota.stackplot.pdf", format='pdf', 
            bbox_inches='tight', dpi=fig.dpi);
plt.show();

# +
# secondary structure information (DSSP)
sites_df = pd.read_csv("data/ALLFAMS_rota.JOY_annot.tab", sep ="\t", index_col=None)
sites_df = sites_df[(sites_df["PDB_AA"] != "-") & (sites_df["homstrad_AA"] != "-")
                   & (sites_df["rota_4"] != "-")]

sb.catplot(x="secondary structure and phi angle", kind="count", palette=colors[1:],
           data=sites_df);
