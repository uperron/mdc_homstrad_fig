# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
from matplotlib.colors import LogNorm
import matplotlib
from matplotlib.gridspec import GridSpec
from mpl_toolkits.axes_grid1 import make_axes_locatable
import seaborn as sb

matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
plt.style.use(['seaborn-white', 'seaborn-paper'])
matplotlib.rc("font", serif='Helvetica Neue')


# -

def plot_homstrad_heatmap(AA_count_df, rota_count_df, val):
    # updated version of the code used for RAM55 heatmaps
    # assumes both matrices are already in the correct order
    # also specific for non-normalized substitution count matrices

    names = rota_count_df.columns

    left = 0.05
    bottom = 0.05
    height = 0.9
    width = 0.9
    offset = 0.05

    numbers = [1, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3]
    ORDER = "GAPCMILVFWYHRKQNDEST"

    twenty_by_twenty = AA_count_df.values
    # expand the 20x20 matrix into a 55x55 matrix
    a_twenty = np.zeros((55,55))
    ci = 0
    for i in range(20):
        cj = 0
        for j in range(20):
            for k in range(numbers[i]):
                for l in range(numbers[j]):
                    a_twenty[ci+k,cj+l] = twenty_by_twenty[i,j]
            cj += numbers[j]
        ci += numbers[i]



    R_ORDER = rota_count_df.columns
    a = rota_count_df.values

    fig = plt.figure(figsize=(12,12))


    axmatrix = fig.add_axes([left,bottom,width,height])

    # custom sequential cmap (same as RAM55 heatmap)
    MAXIMUM = np.max(a)
    cmap = sb.cubehelix_palette(start=.5, rot=-.7, light=.97, as_cmap=True)
    # values are log-normalized
    cax = axmatrix.matshow(a, interpolation='nearest',cmap=cmap, norm=LogNorm(), alpha=1)

    divider = make_axes_locatable(axmatrix)

    # generate two-level ticklabels (residue, chi1_config)
    count = 0
    new_names = R_ORDER
    new_names_again = [] # What the labels say
    new_names_again_x = []
    new_ticks = [] # Where the labels go
    new_ticks_minor = [] # Where the line sticks out
    #tick_alignment = [] # Relative positions
    new_names_20 = []
    new_ticks_20 = []
    for x,y in zip(ORDER, numbers):
        new_ticks_minor.append(count-0.5)
        base = new_names[count][:3]
        new_names_20.append(base)
        if base in ['ALA','GLY']:
            new_ticks.append(count)
            new_ticks_20.append(count)
            new_names_again.append(base)
            new_names_again_x.append(base)
        if base == 'PRO':
                    new_ticks.append(count)
                    new_ticks.append(count+0.5)
                    new_ticks.append(count+1)
                    new_ticks_20.append(count+0.5)
                    new_names_again.append(str(1))
                    new_names_again.append('%s   '%(base))
                    new_names_again.append(str(2))
                    new_names_again_x.append(str(1))
                    new_names_again_x.append('   %s'%(base))
                    new_names_again_x.append(str(2))
        if base not in ['ALA','GLY','PRO']:
            new_ticks.append(count)
            new_ticks.append(count+1)
            new_ticks.append(count+1)
            new_ticks.append(count+2)
            new_ticks_20.append(count+1)
            new_names_again.append(str(1))
            new_names_again.append('%s   '%(base))
            new_names_again.append(str(2))
            new_names_again.append(str(3))
            new_names_again_x.append(str(1))
            new_names_again_x.append('   %s'%(base))
            new_names_again_x.append(str(2))
            new_names_again_x.append(str(3))
        count += y

    axmatrix.set_xticks(new_ticks)
    axmatrix.set_xticks(new_ticks_minor, minor=True )
    axmatrix.set_yticks(new_ticks)
    axmatrix.set_yticks(new_ticks_minor, minor=True )
    axmatrix.tick_params( axis='x', which='major', bottom='off', top='off' )
    axmatrix.tick_params( axis='y', which='major', bottom='off', top='off' )
    axmatrix.tick_params( axis='x', which='minor', direction='out', length=30, bottom='off' )
    axmatrix.tick_params( axis='y', which='minor', direction='out', length=30, bottom='off' )
    axmatrix.set_xticklabels(new_names_again_x,rotation='vertical',fontsize=6)
    axmatrix.set_yticklabels(new_names_again,fontsize=6)

    # add a grid around each submatrix in the 55x55 matrix
    c = 0
    for i,x in enumerate(numbers):
        axmatrix.axvline(c-0.5, color='k', linewidth=.5)
        axmatrix.axhline(c-0.5, color='k', linewidth=.5)
        c += x

    ax_matrix20 = divider.append_axes("right", size="100%", pad=0.5)
    cax2 = ax_matrix20.matshow(a_twenty, interpolation='nearest',cmap=cmap, norm=LogNorm(),alpha=1)

    ax_matrix20.set_xticks(new_ticks_20)
    ax_matrix20.set_xticks(new_ticks_minor, minor=True )
    ax_matrix20.set_yticks(new_ticks_20)
    ax_matrix20.set_yticks(new_ticks_minor, minor=True )
    ax_matrix20.tick_params( axis='x', which='major', bottom='off', top='off' )
    ax_matrix20.tick_params( axis='y', which='major', bottom='off', top='off' )
    ax_matrix20.tick_params( axis='x', which='minor', direction='out', length=30, bottom='off' )
    ax_matrix20.tick_params( axis='y', which='minor', direction='out', length=30, bottom='off' )
    ax_matrix20.set_xticklabels(new_names_20,rotation='vertical',fontsize=6)
    ax_matrix20.set_yticklabels(new_names_20,fontsize=6)

    # add a grid around each cell in the 20x20 matrix
    c = 0
    for i,x in enumerate(numbers):
            ax_matrix20.axvline(c-0.5, color='k', linewidth=.5)
            ax_matrix20.axhline(c-0.5, color='k', linewidth=.5)
            c += x

    # use two separarte colorbars b/c these rotamer state counts are NOT normalized
    ax_cbar = divider.append_axes("left", size="5%", pad=0.7)
    fig.colorbar(cax, cax=ax_cbar)
    ax_cbar2 = divider.append_axes("right", size="5%", pad=0.1)
    fig.colorbar(cax2, cax=ax_cbar2)

    fig.suptitle("solvent accessibility = %s" % (val), y=.27, fontsize=12)
    fig.savefig("figures/acc"+val+"_counts_heatmap.logcmap.pdf",
                format='pdf', bbox_inches='tight', dpi=fig.dpi)
    fig.show()

for val in ["T", "F"]:
    AA_count_f = "data/merged.ALLFAMS_accessibility_"+val+".AA.counts"
    rota_count_f = "data/merged.ALLFAMS_accessibility_"+val+".rota.counts"
    AA_count_df = pd.read_csv(AA_count_f, sep="\t", header=0, index_col=0)
    rota_count_df = pd.read_csv(rota_count_f, sep="\t", header=0, index_col=0)
    plot_homstrad_heatmap(AA_count_df, rota_count_df, val)

# +
# compare with the raw counts from RAM55 
# /nfs/research1/goldman/umberto/bioinfotree/prj/protein_fun_evo/dataset/Rotamers_evo.Pfam.full-lax2/restype_adj_shared_count_matrix.merged
# /nfs/research1/goldman/umberto/bioinfotree/prj/protein_fun_evo/dataset/Rotamers_evo.Pfam.full-lax2/rotamers_adj_shared_count_matrix.merged

# reorder RAM55 matrices
NEW_AA_ORDER = ['GLY', 'ALA', 'PRO', 'CYS', 'MET', 'ILE', 'LEU', 'VAL', 'PHE', 'TRP',
       'TYR', 'HIS', 'ARG', 'LYS', 'GLN', 'ASN', 'ASP', 'GLU', 'SER', 'THR']
OLD_AA_ORDER = sorted(NEW_AA_ORDER)
NEW_rota_ORDER = ['GLY', 'ALA', 'PRO1', 'PRO2', 'CYS1', 'CYS2', 'CYS3', 'MET1', 'MET2', 'MET3', 'ILE1', 'ILE2',
                  'ILE3', 'LEU1', 'LEU2', 'LEU3', 'VAL1', 'VAL2', 'VAL3', 'PHE1', 'PHE2', 'PHE3', 'TRP1', 'TRP2', 'TRP3',
                  'TYR1', 'TYR2', 'TYR3', 'HIS1', 'HIS2', 'HIS3', 'ARG1', 'ARG2', 'ARG3', 'LYS1', 'LYS2', 'LYS3', 'GLN1',
                  'GLN2', 'GLN3', 'ASN1', 'ASN2', 'ASN3', 
                  'ASP1', 'ASP2', 'ASP3', 'GLU1', 'GLU2', 'GLU3', 'SER1', 'SER2', 'SER3', 'THR1', 'THR2', 'THR3']
OLD_rota_ORDER = sorted(NEW_rota_ORDER)
RAM55_AA_count_f = "restype_adj_shared_count_matrix.merged"
RAM55_rota_count_f = "rotamers_adj_shared_count_matrix.merged"
RAM55_AA_count_df = pd.read_csv(RAM55_AA_count_f, sep="\t", header=None, 
                          index_col=None, skiprows=1, usecols=list(range(21))[1:])
RAM55_AA_count_df.columns = OLD_AA_ORDER
RAM55_AA_count_df.index = OLD_AA_ORDER
RAM55_AA_count_df = RAM55_AA_count_df.reindex(NEW_AA_ORDER)
RAM55_AA_count_df = RAM55_AA_count_df[NEW_AA_ORDER]
RAM55_rota_count_df = pd.read_csv(RAM55_rota_count_f, sep="\t", header=None, 
                          index_col=None, skiprows=1, usecols=list(range(56))[1:])
RAM55_rota_count_df.columns = OLD_rota_ORDER
RAM55_rota_count_df.index = OLD_rota_ORDER
RAM55_rota_count_df = RAM55_rota_count_df.reindex(NEW_rota_ORDER)
RAM55_rota_count_df = RAM55_rota_count_df[NEW_rota_ORDER]
AA_count_df = RAM55_AA_count_df
rota_count_df = RAM55_rota_count_df


matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
names = rota_count_df.columns

left = 0.05
bottom = 0.05
height = 0.9
width = 0.9
offset = 0.05

numbers = [1, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3]
ORDER = "GAPCMILVFWYHRKQNDEST"

twenty_by_twenty = AA_count_df.values
# expand the 20x20 matrix into a 55x55 matrix
a_twenty = np.zeros((55,55))
ci = 0
for i in range(20):
	cj = 0
	for j in range(20):
		for k in range(numbers[i]):
			for l in range(numbers[j]):
				a_twenty[ci+k,cj+l] = twenty_by_twenty[i,j]
		cj += numbers[j]
	ci += numbers[i]

R_ORDER = rota_count_df.columns
a = rota_count_df.values

fig = plt.figure(figsize=(12,12))
plt.style.use(['seaborn-white', 'seaborn-paper'])
matplotlib.rc("font", serif='Helvetica Neue')

axmatrix = fig.add_axes([left,bottom,width,height])
#axmatrix20 = fig.add_axes([left+width+0.1,bottom,width,height])

cmap = sb.cubehelix_palette(start=.5, rot=-.7, light=.97, as_cmap=True)
cax = axmatrix.matshow(a, interpolation='nearest',cmap=cmap, norm=LogNorm(), alpha=1)

divider = make_axes_locatable(axmatrix)

# generate two-level ticklabels
count = 0
new_names = R_ORDER
new_names_again = [] # What the labels say
new_names_again_x = []
new_ticks = [] # Where the labels go
new_ticks_minor = [] # Where the line sticks out
#tick_alignment = [] # Relative positions
new_names_20 = []
new_ticks_20 = []
for x,y in zip(ORDER, numbers):
	new_ticks_minor.append(count-0.5)
	base = new_names[count][:3]
	new_names_20.append(base)
	if base in ['ALA','GLY']:
		new_ticks.append(count)
		new_ticks_20.append(count)
		new_names_again.append(base)
		new_names_again_x.append(base)
	if base == 'PRO':
                new_ticks.append(count)
                new_ticks.append(count+0.5)
                new_ticks.append(count+1)
                new_ticks_20.append(count+0.5)
                new_names_again.append(str(1))
                new_names_again.append('%s   '%(base))
                new_names_again.append(str(2))
                new_names_again_x.append(str(1))
                new_names_again_x.append('   %s'%(base))
                new_names_again_x.append(str(2))
	if base not in ['ALA','GLY','PRO']:
		new_ticks.append(count)
		new_ticks.append(count+1)
		new_ticks.append(count+1)
		new_ticks.append(count+2)
		new_ticks_20.append(count+1)
		new_names_again.append(str(1))
		new_names_again.append('%s   '%(base))
		new_names_again.append(str(2))
		new_names_again.append(str(3))
		new_names_again_x.append(str(1))
		new_names_again_x.append('   %s'%(base))
		new_names_again_x.append(str(2))
		new_names_again_x.append(str(3))
	count += y

axmatrix.set_xticks(new_ticks)
axmatrix.set_xticks(new_ticks_minor, minor=True )
axmatrix.set_yticks(new_ticks)
axmatrix.set_yticks(new_ticks_minor, minor=True )
axmatrix.tick_params( axis='x', which='major', bottom='off', top='off' )
axmatrix.tick_params( axis='y', which='major', bottom='off', top='off' )
axmatrix.tick_params( axis='x', which='minor', direction='out', length=30, bottom='off' )
axmatrix.tick_params( axis='y', which='minor', direction='out', length=30, bottom='off' )
axmatrix.set_xticklabels(new_names_again_x,rotation='vertical',fontsize=6)
axmatrix.set_yticklabels(new_names_again,fontsize=6)

c = 0
for i,x in enumerate(numbers):
	axmatrix.axvline(c-0.5, color='k', linewidth=.5)
	axmatrix.axhline(c-0.5, color='k', linewidth=.5)
	c += x

ax_matrix20 = divider.append_axes("right", size="100%", pad=0.5)
MAXIMUM = np.max(a_twenty)
cax2 = ax_matrix20.matshow(a_twenty, interpolation='nearest',cmap=cmap, norm=LogNorm(),alpha=1)

ax_matrix20.set_xticks(new_ticks_20)
ax_matrix20.set_xticks(new_ticks_minor, minor=True )
ax_matrix20.set_yticks(new_ticks_20)
ax_matrix20.set_yticks(new_ticks_minor, minor=True )
ax_matrix20.tick_params( axis='x', which='major', bottom='off', top='off' )
ax_matrix20.tick_params( axis='y', which='major', bottom='off', top='off' )
ax_matrix20.tick_params( axis='x', which='minor', direction='out', length=30, bottom='off' )
ax_matrix20.tick_params( axis='y', which='minor', direction='out', length=30, bottom='off' )
ax_matrix20.set_xticklabels(new_names_20,rotation='vertical',fontsize=6)
ax_matrix20.set_yticklabels(new_names_20,fontsize=6)

c = 0
for i,x in enumerate(numbers):
        ax_matrix20.axvline(c-0.5, color='k', linewidth=.5)
        ax_matrix20.axhline(c-0.5, color='k', linewidth=.5)
        c += x

ax_cbar = divider.append_axes("left", size="5%", pad=0.7)
fig.colorbar(cax, cax=ax_cbar)
ax_cbar2 = divider.append_axes("right", size="5%", pad=0.1)
fig.colorbar(cax2, cax=ax_cbar2)

fig.suptitle("figures/RAM55 raw counts", y=.27, fontsize=12)
fig.savefig("figures/RAM55_counts_heatmap.logcmap.pdf", format='pdf', bbox_inches='tight', dpi=fig.dpi)
fig.show()
