# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import pandas as pd
import re

# scrape a list of all family ids from the homstrad website
# this is input for https://bitbucket.org/uperron/mdc_homstrad
arr = []
with open("List_of_families.html", "r") as f:
    for line in f:
        try:
            arr.append(re.sub("(^family=)|(\"$)", "", re.search('family=.*"', line).group(0)))
        except:
            continue
id_df = pd.DataFrame(arr, columns=['family_id'])
# -

# scrape all other info avalable on each family
info_df = pd.DataFrame(pd.read_html("List_of_families.html", header=0)[0].values)
info_df.columns = "idx family_name number_of_structures average_length average_%ID".split()
info_df.drop('idx', axis=1, inplace=True)

# and concat the two dfs
df_out = pd.concat([id_df, info_df], axis=1)
df_out.to_csv("data/homstrad_allfamilies.scraped.tab", sep="\t", index=False)
