# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import numpy as np
import pandas as pd
from collections import defaultdict

AA_count_f = "merged.ALLFAMS_accessibility_F.AA.counts"
AA_count_df = pd.read_csv(AA_count_f, sep="\t", header=0, index_col=0)
rota_count_f = "merged.ALLFAMS_accessibility_F.rota.counts"
rota_count_df = pd.read_csv(rota_count_f, sep="\t", header=0, index_col=0)

state_dict = defaultdict(list)
for aa in AA_count_df.columns:
    state_dict[aa].extend([r for r in rota_count_df.columns if aa in r])
masking_dict = {}
for r in rota_count_df.columns:
    masking_dict[r] = [aa for aa in AA_count_df.columns if aa in r][0]
    
def add_pseudocounts(count_df):
    arr = count_df.values
    arr = np.where(arr==0.0, 1, arr)
    out_df = pd.DataFrame(arr, columns=count_df.columns)
    out_df.index = count_df.index
    return out_df

def calc_IRM(count_df):
    # compute the IRM and equilibrium frequencies from the counts
    IRM_df = count_df.copy()
    f_arr = [[]]
    for s1 in count_df.columns:
        s =  count_df.loc[s1, :].sum()
        f_arr[0].append(s)
        for s2 in count_df.columns:
            q = count_df.loc[s1, s2] / s
            IRM_df.loc[s1, s2] = q
    S = sum(f_arr[0])>>>>
    f_arr[0] = [n / S for n in f_arr[0]]
    f_df = pd.DataFrame(f_arr, columns=count_df.columns)
    return IRM_df, f_df

AA_count_df = add_pseudocounts(AA_count_df)
rota_count_df = add_pseudocounts(rota_count_df)

# "Dayhoff" normalization for the rotamer substitution counts.
# This is meant to account for the fact that some states are not filtere by B_factor 
# and have therefore higher counts in the rotamer matrix (i.e. ALA, GLY)
# also filtering by resolution and B_factor might select against residues 
# that are rarer in the core region.
rota_Dnorm_counts_df = rota_count_df.copy()
for a1 in AA_count_df.columns:
    for a2 in AA_count_df.columns:
        # slice the corresponding rotamer submatrix
        rota_subm = rota_count_df.loc[state_dict[a1],state_dict[a2]]
        # The value of each cell in the submatrix is divided by the sum total of the submatrix, 
        # then multiplied by the value in the file_norm cell
        rota_subm = rota_subm / rota_subm.sum().sum() * AA_count_df.loc[a1, a2]
        rota_Dnorm_counts_df.loc[state_dict[a1],state_dict[a2]] = rota_subm
        


rota_Dnorm_IRM_df, rota_Dnorm_freqs_df = calc_IRM(rota_Dnorm_counts_df)
AA_IRM_df, AA_freqs_df = calc_IRM(AA_count_df)


# +
# Scale the rotamer IRM so that , at equilibrium,
# it will result on avg in 1 AA change per unit of time.
def rho_scale(IRM_df, freqs_df):
    # compute rho
    rho = 0
    for r1 in IRM_df.columns:
        for r2 in IRM_df.columns:
            if r1 != r2:
                rho = IRM_df.loc[r1, r2] * freqs_df.loc[0,r1] + rho
    print("Rho scaling factor = %f" % (rho))

    IRM_Q_df = IRM_df.copy()
    # apply the rho scaling
    for r1 in IRM_df.columns:
        for r2 in IRM_df.columns:
            IRM_Q_df.loc[r1, r2] = IRM_df.loc[r1, r2] / rho
    return IRM_Q_df

rota_Dnorm_IRM_Q_df = rho_scale(rota_Dnorm_IRM_df, rota_Dnorm_freqs_df)
AA_IRM_Q_df = rho_scale(AA_IRM_df, AA_freqs_df)

#compute super-rho
super_rho = .0
for r1 in rota_Dnorm_IRM_Q_df.columns:
    a1 = masking_dict[r1]
    for r2 in [r for r in rota_Dnorm_IRM_Q_df.columns if a1 not in r]:
        super_rho = rota_Dnorm_IRM_Q_df.loc[r1, r2] * rota_Dnorm_freqs_df.loc[0,r1] + super_rho
print("Super_rho scaling factor = %f" % (super_rho))

# apply the super-rho scaling
rota_Dnorm_IRM_superQ_df = rota_Dnorm_IRM_Q_df.copy()
for r1 in rota_Dnorm_IRM_Q_df.columns:
    for r2 in rota_Dnorm_IRM_Q_df.columns:
        rota_Dnorm_IRM_superQ_df.loc[r1, r2] = rota_Dnorm_IRM_Q_df.loc[r1, r2] / super_rho


# +
# compute exchangeabilities
# Sij = Qij / Pj with Pj being the equilibrium frequency of state j
# main diagonal cells are 0

def calc_exchang(IRM_df, freqs_df):
    exchang_df = IRM_df.copy()
    for r1 in IRM_df.columns:
        for r2 in IRM_df.columns:
            if r1 != r2:
                # Sij = Qij / Pj
                s = IRM_df.loc[r1, r2] / freqs_df.loc[0, r2]
            else:
                # set the diagonal cells to .0
                s = .0
            exchang_df.loc[r1, r2] = s
    return exchang_df

rota_Dnorm_exchang_superQ_df = calc_exchang(rota_Dnorm_IRM_superQ_df, rota_Dnorm_freqs_df)
AA_exchang_Q_df = calc_exchang(AA_IRM_Q_df, AA_freqs_df)
# -

AA_exchang_Q_df
